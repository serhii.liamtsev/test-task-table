import axios from 'axios'
import config from '../config.js'

const apiRoot = axios.create({
  baseURL: config.API_ROOT
})

export {
  apiRoot
}
