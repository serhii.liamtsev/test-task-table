export default api => ({
  getAll: params => api.get('entries', { params }),
  getCategories: () => api.get('categories')
})
