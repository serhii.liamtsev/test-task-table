import entries from './services/entries.js'
import { apiRoot } from './instances.js'

export const api = {
  entries: entries(apiRoot)
}
